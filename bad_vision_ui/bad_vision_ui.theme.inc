<?php

/**
 * @param $vars
 */
function theme_bad_vision_ui_settings_form($vars) {
  return _bad_vision_ui_render_table($vars);
}

/**
 * @param $vars
 * @return string
 */
function _bad_vision_ui_render_table($vars) {
  $form = $vars['form'];

  $children = array_intersect_key($form['container'], element_children($form['container']));

  foreach ($children as $key => $element) {
    if (isset($element['table_' . $key])) {
      $table = $element['table_' . $key];
      $rows = array();
      foreach (element_children($table) as $value) {
        $row = array();
        $input = $table[$value];
        if (isset($input['label']) && isset($input['value']) && isset($input['weight']) && isset($input['delete'])) {
          $input['weight']['#attributes']['class'][] = 'bad-vision-ui-weight';
          $row[] = array('data' => $input['label']);
          $row[] = array('data' => $input['value']);
          $row[] = array('data' => $input['weight']);
          $row[] = array('data' => $input['delete']);
          $rows[] = array(
            'data' => $row,
            'class' => array('draggable'),
          );
        }
      }

      unset($form['container'][$key]['table_' . $key]);

      $headers = array(t('Label'), t('Value'), t('Weight'), t('Delete'));
      $output = theme('table', array(
        'header' => $headers,
        'rows' => $rows,
        'attributes' => array(
          'id' => 'bad-vision-ui-' . $key . '-table',
        ),
      ));
      
      drupal_add_tabledrag('bad-vision-ui-' . $key . '-table', 'order', 'sibling', 'bad-vision-ui-weight');

      $form['container'][$key]['table_' . $key] = array(
        '#markup' => $output,
      );
      
    }
  }

  return drupal_render_children($form);
}
