<?php

/**
 * Build bad_vision settings page.
 */
function bad_vision_ui_settings() {
  //Update cache bad_vision_settings always when update page
  bad_vision_cache_clear('bad_vision_settings');

  //Trying to get bad_vision variants
  if ($bv_variants = bad_vision_get_variants_all()) {
    bad_vision_cache_set('bad_vision_settings', $bv_variants);
  }

  //Get bad_vision_ui_settings_form
  $settings_form = drupal_get_form('bad_vision_ui_settings_form');

  return drupal_render($settings_form);
}

/**
 * Build bad_vision settings form.
 */
function bad_vision_ui_settings_form($form, &$form_state) {
  $form = array();

  $form['#attached']['css'][] = drupal_get_path('module', 'bad_vision_ui') . '/css/bad_vision_ui.css';

  $form['#prefix'] = '<div id="bad-vision-ui-settings-form-wrapper">';
  $form['#suffix'] = '</div>';



  $form['container'] = array(
    '#type' => 'vertical_tabs',
    '#default_tab' => isset($form_state['values']['bv_type']) ? 'bad-vision-tab-type-' . $form_state['values']['bv_type'] : '',
  );

  //Get All bad_vision types
  $bv_types = bad_vision_ui_get_types_all();

  $form_state['bv_types'] = $bv_types;

  //Build "containers" and  "enable checkboxs" for types
  foreach ($bv_types as $type_id => $type) {

    //Build containers for types
    $form['container'][$type_id] = array(
      '#type' => 'fieldset',
      '#title' => $type->name,
      '#collapsible' => TRUE,
      '#group' => 'group_tabs',
      '#attributes' => array(
        'class' => array('bad-vision-type-' . $type_id),
        'id' => 'bad-vision-tab-type-' . $type_id,
      ),
      '#weight' => $type->weight,
    );

    //Build enable checkboxs for types
    $form['container'][$type_id]['enable_' . $type_id] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable'),
      '#default_value' => $type->enable,
    );

    if ($type->variants) {

      $form['container'][$type_id]['table_' . $type_id] = array(
        '#tree' => TRUE,
      );

      //Button allows add new empty row.
      $form['container'][$type_id]['more_' . $type_id] = array(
        '#name' => 'more_' . $type_id,
        '#type' => 'submit',
        '#value' => t('Add more'),
        '#weight' => 10,
        '#ajax' => array(
          'callback' => '_bad_vision_ui_form_settings_ajax',
          'wrapper' => 'bad-vision-ui-settings-form-wrapper',
        ),
      );
    }
  }

  //Get all bad_vision $variants
  $variants = bad_vision_cache_get('bad_vision_settings');

  //Build tables for bad_vision variants
  if ($variants) {
    foreach ($variants as $type => $variants_of_type) {
      foreach ($variants_of_type as $id => $variant) {
        $form['container'][$type]['table_' . $type][$variant->id]['label'] = array(
          '#type' => 'textfield',
          '#default_value' => isset($variant->label) ? filter_xss($variant->label) : '',
          '#maxlength' => 100,
        );

        $form['container'][$type]['table_' . $type][$variant->id]['value'] = array(
          '#type' => 'textfield',
          '#default_value' => isset($variant->value) ? filter_xss($variant->value) : '',
          '#maxlength' => 100,
        );

        $form['container'][$type]['table_' . $type][$variant->id]['weight'] = array(
          '#type' => 'textfield',
          '#default_value' => isset($variant->weight) ? filter_xss($variant->weight) : '',
        );

        $form['container'][$type]['table_' . $type][$variant->id]['delete'] = array(
          '#name' => 'delete_' . $variant->type . '_' . $variant->id,
          '#type' => 'submit',
          '#value' => t('Delete'),
          '#ajax' => array(
            'callback' => '_bad_vision_ui_form_settings_ajax',
            'wrapper' => 'bad-vision-ui-settings-form-wrapper',
            'progress' => array(
              'type' => 'throbber',
              'message' => '',
            ),
          ),
        );
      }
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Validate function for bad_vision ui settings form.
 */
function bad_vision_ui_settings_form_validate($form, &$form_state) {
  //Bad_vision types
  $bv_types = $form_state['bv_types'];

  // Do not validate form if DELETE button was submitted.
  if (array_search(t('Delete'), $form_state['values'], TRUE)) {
    return;
  }

  foreach ($form_state['values'] as $key => $value) {
    if (is_array($value)) {
      //Get type id
      $type = drupal_substr($key, 6);
      //Get human name of type
      $name = $bv_types[$type]->name;

      foreach ($value as $id => $variant) {
        if (empty($variant['label'])) {
          form_set_error('table_' . $type . '][' . $id . '][label', t('@name: "label" should contain value.', array('@name' => $name)));
        }
        if (empty($variant['value'])) {
          form_set_error('table_' . $type . '][' . $id . '][value', t('@name: "Value" should contain value.', array('@name' => $name)));
        }
      }
    }
  }
}

function bad_vision_ui_settings_form_submit($form, &$form_state) {

  // Remove row when DELETE button submitted.  
  if ($form_state['clicked_button']['#value'] == t('Delete')) {
    $delete = $form_state['clicked_button']['#name'];
    $deleted_key = drupal_substr($delete, 7);
    //As type and id
    $deleted_key = explode('_', $deleted_key);

    //Remove info about this variant out of form and form_state
    unset($form_state['values']['table_' . $deleted_key[0]][$deleted_key[1]]);
    unset($form['container'][$deleted_key[0]]['table_' . $deleted_key[0]][$deleted_key[1]]);

    //Remove info about this variant out of cache
    bad_vision_cache_remove_id_of_type('bad_vision_settings', $deleted_key[0], $deleted_key[1]);

    //Save curent bad_vision type for active vertical_tabs
    $form_state['values']['bv_type'] = $deleted_key[0];

    //Rebuild form
    $form_state['rebuild'] = TRUE;
  }

  // Add row when MORE button submitted.
  if ($form_state['clicked_button']['#value'] == t('Add more')) {
    $more = $form_state['clicked_button']['#name'];
    $more_key = drupal_substr($more, 5);

    //Add empty row in cache bad_vision_settings
    bad_vision_add_variant_of_type('bad_vision_settings', $more_key);

    //Save curent bad_vision type for active vertical_tabs
    $form_state['values']['bv_type'] = $more_key;

    //Rebuild form
    $form_state['rebuild'] = TRUE;
  }

  //Save bad_vision variants in data base
  if ($form_state['clicked_button']['#value'] == t('Save')) {

    //Remove current bad_vision settings
    db_truncate('bad_vision_variants')->execute();

    //Record new bad_vision settings in data base
    foreach ($form_state['values'] as $key => $value) {
      //For tables
      if (is_array($value)) {
        //Get type id
        $type = drupal_substr($key, 6);

        //bad_vision variants for current "$value-type"
        foreach ($value as $id => $variant) {
          //Add new row in table "bad_vision_variants"
          $row = new stdClass();
          $row->type = $type;
          $row->label = $variant['label'];
          $row->value = $variant['value'];
          $row->weight = $variant['weight'];
          drupal_write_record('bad_vision_variants', $row);
        }
      }

      //For enable type
      if (drupal_substr($key, 0, 6) == 'enable') {
        //Get type id
        $type = drupal_substr($key, 7);

        //Update field enable for current type in table "bad_vision_types""
        db_update('bad_vision_types')
          ->fields(array('enable' => $value))
          ->condition('type', $type)
          ->execute();
      }
    }

    //Shom success message
    drupal_set_message(t('Your settings have been successfully saved'), 'status');
  }
}

/**
 * Ajax callback for ADD MORE button.
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function _bad_vision_ui_form_settings_ajax($form, &$form_state) {
  // Simple form reload.
  return $form;
}
