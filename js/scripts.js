(function ($) {
  Drupal.behaviors.badVision = {
    attach: function (context, settings) {

      $('body', context).once('badVision', function () {

        //Устанавливаем опции для кук
        cookieOptions = {path: Drupal.settings.basePath, expires: 365};

        //Объект доступных методов для badVersion
        var methods = {
          //Инициализация объекта
          init: function (options) {

            //Запускаем первый раз
            $(this).click(function (event) {
              event.preventDefault();

              //Устанавливаем куку ссылка на обычную версию
              Cookies.set('bv-link', '#' + $(this).attr('id'), cookieOptions);

              //Забираем данные с сервера и запускаем плагин
              runbadVersion();
            });

            //Если есть куки подготавливаем всё исходя из них
            if (Cookies.get('bv-on')) {
              //Add bad_vision styles
              $("head").append($('<link rel="stylesheet" id="bv-style" href="' + Drupal.settings.basePath + options['bv-style' ] + '" type="text/css" media="screen" />'));

              //Add bad_vision wrapper
              $('body').children().wrapAll('<div id="bad-vision-wrapper"></div>');
              
              //Add table settings
              $('body').prepend('<div id="bad-vision-table-wrapper">' + options['bv-table'] + '</div>');

              //Запускаем изменения с параметрами-куками если они есть
              if (options['bv-cookies']) {
                $.fn.badVersion('changeStyles', options['bv-cookies']);
              }

              //hide link on
              var idBvOn = Cookies.get('bv-link');
              $(idBvOn).hide();

              //Клонируем ссылку в таблицу и меняем ей текст
              commonLink = $(idBvOn).clone().appendTo('#bad-vision-table-wrapper');
              commonLink.text(options['bv-common-text']).show();

              //При нажатии на склонированную ссылку
              //Выключаем badVersion
              commonLink.click(function (event) {
                event.preventDefault();
                $.fn.badVersion('destroy');
              });

              //Обработка нажатий вариантов
              $('body').on('click', '#bad-vision-table a', function (event) {
                event.preventDefault();

                var css = $(this).attr('bv-css');
                var value = $(this).attr('bv-value');
                var variants = $(this).attr('bv-variants');

                //Формируем объект
                var bvNewVariant = {}
                var typeId = $(this).attr('bv-type-id');
                bvNewVariant[typeId] = {}
                bvNewVariant[typeId]['css'] = css;
                bvNewVariant[typeId]['value'] = value;
                bvNewVariant[typeId]['variants'] = variants;

                //Устанавливаем свежую куку
                Cookies.set('bv-' + css, value, cookieOptions);

                //Запускаем изменения
                $.fn.badVersion('changeStyles', bvNewVariant);
              });
            }
          },
          //Завершаем работу плагина: удаляем все куки и перезагружаем страницу
          destroy: function () {
            //Remove All bv cookies
            document.cookie.split(";").forEach(function (el) {
              el = el.split("=")[0].trim();
              if (!el.indexOf('bv-')) {
                Cookies.remove(el, cookieOptions);
              }
            });

            location.reload();
          },
          //Изменяем стили елементов исходя из предоставленных куков или новых значений 
          changeStyles: function (bvCookies) {

            var noChangeStyles = '#admin-menu, #admin-menu *, #toolbar, #toolbar *, #bv-on';

            //Перебираем все переданные куки или значения для изменения
            $.each(bvCookies, function (i, cookie) {

              //Если допустимы варианты' применяем ко всем
              //иначе только к изображениям
              if (cookie['variants'] == 1) {
                selectTarget = '*';
              } else {
                selectTarget = 'img';
              }

              var css = cookie['css'];

              //@TODO For reset
              //Сохраняем оригинальные свойства элементов если их нет    
              $('#bad-vision-wrapper ' + selectTarget).not(noChangeStyles).each(function (y, elem) {
                if (!$(elem).attr('original-' + css)) {
                  $(elem).attr('original-' + css, $(elem).css(css));
                }
              });

              //Устанавливаем значения свойств элементов исходя из кук или переданных новых значений.
              //НЕЛЬЗЯ делать в предыдущем цикле
              //из-за наследования свойств
              $('#bad-vision-wrapper ' + selectTarget).not(noChangeStyles).each(function (y, elem) {
                var objCss = {};
                objCss[css] = cookie['value'];
                $(elem).css(objCss);
              });

            });
          }
        };

        $.fn.badVersion = function (method) {
          if (methods[method]) {
            return methods[ method ].apply(this, Array.prototype.slice.call(arguments, 1));
          } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
          } else {
            $.error('Method named ' + method + 'does not exist for jQuery.badVersion');
          }
        };

        //Если включена версия - делаем запрос на сервер - выбираем все данные,
        //затем запускаем плагин - показываем версию, иначе просто активируем
        if (Cookies.get('bv-on')) {
          runbadVersion();
        } else {
          //Инициализируем плагин
          $('#bv-on').badVersion();
        }

        //Посылает запрос на сервер, при успешном получении данных
        //запускает плагин используя полученные данные как параметры
        function runbadVersion() {
          var settings = {
            url: Drupal.settings.basePath + 'bad-vision',
            success: function (response, status) {
              //Флаг ошибки доступа
              var error = false;

              //Ищем ошибку доступа
              $.each(response, function (i, elem) {
                if (elem['command'] == 'alert') {
                  error = elem['text'];
                  return false;
                }
              });

              //Если доступ разрешен - запускаем плагин с полученными опциями
              if (!error) {
                options = {
                  'bv-style': response['bad_vision'].bv_style,
                  'bv-table': response['bad_vision'].bv_table,
                  'bv-bad-text': response['bad_vision'].bv_bad_text,
                  'bv-common-text': response['bad_vision'].bv_common_text,
                };

                //Обходим все доступные типы
                //и ищем соответствующие куки,
                //если они определены, то добавляем их к @options
                $.each(response['bad_vision'].bv_types, function (i) {
                  if (Cookies.get('bv-' + this['css'])) {
                    if (!options['bv-cookies']) {
                      options['bv-cookies'] = {};
                    }
                    options['bv-cookies'][i] = {};
                    options['bv-cookies'][i]['css'] = this['css'];
                    options['bv-cookies'][i]['value'] = Cookies.get('bv-' + this['css']);
                    options['bv-cookies'][i]['variants'] = this['variants'];
                  }
                });

                //Устанавливаем куку ключ запуска
                Cookies.set('bv-on', 'on', cookieOptions);

                //Запускаем плагин с параметрами
                $.fn.badVersion('init', options);
                Drupal.ajax.prototype.success.apply(this, arguments);
              } else {
                alert(error);
              }
            },
          };
          var ajax = new Drupal.ajax(false, false, settings);
          ajax.eventResponse(ajax, {});
        }
      });

    }
  };
}(jQuery));
